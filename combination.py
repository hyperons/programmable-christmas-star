from star import Star
from time import sleep
from gpiozero.tools import random_values
from signal import pause

star = Star(pwm=True)

step = 0.3
count = 0

try:
    leds = star.leds
    while True:
        for x in range(5):
            star.outer.off()
            star.inner.off()
            for led in leds:
                led.pulse()
                sleep(.2)

        for x in range(1):
            star.outer.off()
            star.inner.off()
            while True:
               if(count%26!=0):
                   leds[count%26].on()
                   sleep(step)
                   leds[count%26].off()
            
               count += 1
               step = step*0.99
        
               if(step <= 0.0001):
                   count = 0
                   step = 0.3
                   break

        for x in range(5):
            star.inner.on()
            star.outer.off()
            sleep(.5)
            star.toggle()
            sleep(.5)
except KeyboardInterrupt:
    star.off()
    star.close()
